//
//  SecondViewController.swift
//  TableViews Rock!
//
//  Created by Musipatla,Hyndavi on 2/19/19.
//  Copyright © 2019 Musipatla,Hyndavi. All rights reserved.
//

import UIKit

class TouristSitesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var site = ["Goa", "Hyderabad", "Delhi", "Mumbai", "Bangalore"]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return site.count
            
        } else {
            return -1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sites")!
        cell.textLabel?.text = site[indexPath.row]
        return cell
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

